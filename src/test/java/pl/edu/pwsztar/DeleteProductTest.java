package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DeleteProductTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setup(){
        shoppingCart = new ShoppingCart();
    }

    @Test
    void shouldReturnFalseWhenProductDoesNotExist(){
        //given empty cart
        //when non existent product is deleted
        boolean result = shoppingCart.deleteProducts("non existent product",3);

        //then result should be false
        assertFalse(result);
    }

    @Test
    void shouldReturnFalseWhenQuantityIsLowerThanOne(){
        //given cart with one product
        shoppingCart.addProducts("SteelSeries Arctis Pro Wireless czarne",1270,3);

        //when quantity 0 or lower is deleted
        boolean result1 = shoppingCart.deleteProducts("SteelSeries Arctis Pro Wireless czarne",0);
        boolean result2 = shoppingCart.deleteProducts("SteelSeries Arctis Pro Wireless czarne",-2);

        //then quantity should remain 3 and results should be false
        assertEquals(3,shoppingCart.getQuantityOfProduct("SteelSeries Arctis Pro Wireless czarne"));
        assertFalse(result1);
        assertFalse(result2);
    }

    @Test
    void shouldChangeProductQuantity(){
        //given cart with one product
        shoppingCart.addProducts("SteelSeries Arctis Pro Wireless czarne",1270,3);

        // when 2 pieces are deleted
        boolean result = shoppingCart.deleteProducts("SteelSeries Arctis Pro Wireless czarne",2);

        //then the quantity should be 1 and result true
        assertEquals(1,shoppingCart.getQuantityOfProduct("SteelSeries Arctis Pro Wireless czarne"));
        assertTrue(result);
    }

    @Test
    void shouldCompletelyDeleteProduct(){
        //given cart with one product
        shoppingCart.addProducts("SteelSeries Arctis Pro Wireless czarne",1270,3);

        // when 3 pieces are deleted
        boolean result = shoppingCart.deleteProducts("SteelSeries Arctis Pro Wireless czarne",3);

        //then the quantity should be 0 and result true and name of product should not be in a list of names
        assertEquals(0,shoppingCart.getQuantityOfProduct("SteelSeries Arctis Pro Wireless czarne"));
        assertTrue(result);
        assertFalse(shoppingCart.getProductsNames().contains("SteelSeries Arctis Pro Wireless czarne"));
    }
}
