package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GetProductNamesTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setup(){
        shoppingCart = new ShoppingCart();
    }

    @Test
    void shouldReturnEmptyListWhenTheCartIsEmpty(){
        //given empty cart
        //when there is no products and product names are get
        List<String> result = shoppingCart.getProductsNames();

        //the result should empty list
        assertTrue(result.isEmpty());
    }

    @Test
    void shouldGetProductNames(){
        //given cart with products
        shoppingCart.addProducts("Razer Deathadder Essential",150,4);
        shoppingCart.addProducts("Razer DeathAdder Elite",220, 5);
        shoppingCart.addProducts("Razer Naga Trinity", 400,6);

        //when product names are get
        List<String> result = shoppingCart.getProductsNames();

        //then list should contain product names and size should equal number of names
        assertTrue(result.contains("Razer Deathadder Essential"));
        assertTrue(result.contains("Razer DeathAdder Elite"));
        assertTrue(result.contains("Razer Naga Trinity"));
        assertEquals(3,result.size());
    }
}
