package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class GetQuantityTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setup(){
        shoppingCart = new ShoppingCart();
    }

    @Test
    void shouldGetQuantity(){
        //given AMD Ryzen 7 3700X of quantity 3
        String name ="AMD Ryzen 7 3700X";
        int price = 1500;
        int quantity = 3;
        shoppingCart.addProducts(name,price,quantity);

        //when the quantity is checked
        int result = shoppingCart.getQuantityOfProduct(name);

        //then result should be 3
        assertEquals(quantity,result);
    }

    @Test
    void shouldGetZero(){
        //given empty cart
        //when quantity of non existent product is being checked
        int result = shoppingCart.getQuantityOfProduct("");

        //then result should be 0
        assertEquals(0,result);
    }
}
