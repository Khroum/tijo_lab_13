package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class GetSumProductPricesTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setup(){
        shoppingCart = new ShoppingCart();
    }

    @Test
    void shouldReturnZeroWithEmptyCart(){
        //given empty shopping cart
        //when total is returned
        int result = shoppingCart.getSumProductsPrices();

        //then result should be 0
        assertEquals(0,result);
    }

    @Test
    void shouldReturnTotalPrices(){
        //given cart with a few products
        shoppingCart.addProducts("MSI Geforce RTX 2060 SUPER GAMING X 8GB GDDR6",2240,3);
        shoppingCart.addProducts("MSI Geforce RTX 2070 SUPER GAMING X 8GB GDDR6",2750,4);
        shoppingCart.addProducts("MSI Geforce RTX 2080 SUPER GAMING X TRIO 8GB GDDR6",3900,2);

        //when the total price is returned 6720,11000, 7800
        int result = shoppingCart.getSumProductsPrices();

        //then the result should equal 2240x3 + 2750*4 + 3900*2 = 25520
        assertEquals(25520,result);
    }
}
