package pl.edu.pwsztar;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AddProductTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setup(){
        shoppingCart = new ShoppingCart();
    }

    @Test
    void addProductToEmptyCart(){
        //given empty cart and product data
        String name ="AMD Ryzen 7 3700X";
        int price = 1500;
        int quantity = 3;

        //when the product is added to an empty cart
        boolean result = shoppingCart.addProducts(name,price,quantity);

        //then the result should be true
        assertTrue(result);
    }

    @Test
    void shouldNotAddProductWithIncorrectData(){
        //given empty cart and incorrect data
        String emptyName = "";
        int zeroPrice = 0;
        int minusQuantity = -1;

        // and correct data
        String name ="AMD Ryzen 7 3700X";
        int price = 1500;
        int quantity = 3;

        //when the products are being added
        boolean result1 = shoppingCart.addProducts(emptyName,price,quantity);
        boolean result2 = shoppingCart.addProducts(null,price,quantity);
        boolean result3 = shoppingCart.addProducts(name,zeroPrice,quantity);
        boolean result4 = shoppingCart.addProducts(name,price,minusQuantity);

        //all results should be false
        assertFalse(result1);
        assertFalse(result2);
        assertFalse(result3);
        assertFalse(result4);
    }

    @Test
    void shouldNotAddProductBecauseOfTheLimit(){
        //given nearly full cart
        String name1 = "AMD Ryzen 7 3700X";
        int price1 = 1500;
        int quantity1 = 497;
        shoppingCart.addProducts(name1,price1,quantity1);

        //and product which will exceed quantity limit
        String name2 = "AMD Ryzen 5 3600";
        int price2 = 860;
        int quantity2 = 4;

        //when the product is added
        boolean result = shoppingCart.addProducts(name2,price2,quantity2);

        //then te result should be false
        assertFalse(result);
    }

    @Test
    void shouldChangeTheQuanity(){
        //given cart with product and the same product to add
        String name ="AMD Ryzen 7 3700X";
        int price = 1500;
        int quantity1 = 3;
        int quantity2 = 4;
        shoppingCart.addProducts(name,price,quantity1);

        //when the product is added, the quantity should change
        boolean resultIsAdded = shoppingCart.addProducts(name,price,quantity2);
        int resultQuantity = shoppingCart.getQuantityOfProduct(name);

        //then it should add product and return the new quantity which should be sum of quantity1 and quantity2
        assertTrue(resultIsAdded);
        assertEquals(quantity1+quantity2,resultQuantity);
    }

    @Test
    void shouldNotChangeTheQuantityBecausePricesAreDifferent(){
        //given cart with product and the same product to add but with different price
        String name ="AMD Ryzen 7 3700X";
        int price1 = 1500;
        int price2 = 1400;
        int quantity1 = 3;
        int quantity2 = 4;
        shoppingCart.addProducts(name,price1,quantity1);

        //when the product is added
        boolean resultIsAdded = shoppingCart.addProducts(name,price2,quantity2);
        int resultQuantity = shoppingCart.getQuantityOfProduct(name);

        //then it should not add the product and quantity should not change
        assertFalse(resultIsAdded);
        assertEquals(quantity1,resultQuantity);
    }

}
