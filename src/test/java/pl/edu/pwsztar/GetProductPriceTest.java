package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class GetProductPriceTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setup(){
        shoppingCart = new ShoppingCart();
    }

    @Test
    void shouldReturnZeroWhenProductDoesNotExist(){
        //given empty cart
        //when price is checked
        int result = shoppingCart.getProductPrice("non existent product");

        //then result should be 0
        assertEquals(0,result);
    }

    @Test
    void shouldReturnPriceOfTheProduct(){
        //given cart with product
        shoppingCart.addProducts("SteelSeries Apex PRO TKL OmniPoint",900,3);

        //when the price of product is checked
        int result = shoppingCart.getProductPrice("SteelSeries Apex PRO TKL OmniPoint");

        //then result should be price of the product
        assertEquals(900,result);
    }
}
