package pl.edu.pwsztar;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {
    private final SelectedProductRepository repository;

    public ShoppingCart() {
        this.repository = new SelectedProductRepository();
    }

    public boolean addProducts(String productName, int price, int quantity) {
        if(cannotAddProduct(productName, price, quantity)){
            return false;
        }

        final Product productToSave = new Product(productName,price,quantity);
        Optional<Product> productExistent = repository.findByName(productName);

        return productExistent
                .map(existentProduct -> increaseQuantityOfExistentProduct(productToSave, existentProduct))
                .orElse(saveNewNonExistentProduct(productToSave));
    }

    private Boolean increaseQuantityOfExistentProduct(Product productToSave, Product existentProduct) {
        if (hasDifferentPrice(productToSave, existentProduct)){
            return false;
        }

        repository.increaseQuantityOfProduct(existentProduct,productToSave.getQuantity());
        return true;
    }

    private boolean saveNewNonExistentProduct(Product productToSave){
        repository.save(productToSave);
        return true;
    }

    private boolean hasDifferentPrice(Product product1, Product product2) {
        return product1.getPrice() != product2.getPrice();
    }


    private boolean cannotAddProduct(String productName, int price, int quantity) {
        return isDataIncorrect(productName,price,quantity) || hasReachedQuantityLimit(quantity);
    }

    private boolean isDataIncorrect(String name, int price, int quantity){
        return name == null || name.isEmpty() || price <= 0 || quantity <= 0;
    }

    private boolean hasReachedQuantityLimit(int quantityOfNewProduct){
        int quantityAfter = repository.getQuantityOfAllProducts() + quantityOfNewProduct;
        return quantityAfter > PRODUCTS_LIMIT;
    }

    public boolean deleteProducts(String productName, int quantity) {
        if (quantityLowerThanOne(quantity)){
            return false;
        }

        Optional<Product> searchedProduct = repository.findByName(productName);
        return searchedProduct.map(product -> {
            if(product.getQuantity()<quantity){
                return false;
            }
            else if(product.getQuantity()==quantity){
                repository.deleteByName(productName);
                return true;
            }
            repository.decreaseQuantityOfProduct(product,quantity);
            return true;
        }).orElse(false);
    }

    private boolean quantityLowerThanOne(int quantity) {
        return quantity<1;
    }

    public int getQuantityOfProduct(String productName) {
        Optional<Product> searchedProduct = repository.findByName(productName);
        return  searchedProduct
                .map(Product::getQuantity)
                .orElse(0);
    }

    public int getSumProductsPrices() {
        return repository.getTotalPrice();
    }

    public int getProductPrice(String productName) {
        Optional<Product> searchedProduct = repository.findByName(productName);

        return searchedProduct
                .map(Product::getPrice)
                .orElse(0);
    }

    public List<String> getProductsNames() {
        return repository
                .findAll()
                .stream()
                .map(Product::getName)
                .collect(Collectors.toList());
    }
}
