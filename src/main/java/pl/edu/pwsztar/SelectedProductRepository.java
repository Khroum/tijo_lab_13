package pl.edu.pwsztar;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class SelectedProductRepository {
    private final List<Product> products = new LinkedList<Product>();

    void save(Product productToSave){
        products.add(productToSave);
    }

    void deleteByName(String productName){
        findByName(productName).ifPresent(products::remove);
    }

    Optional<Product> findByName(String productName){
        return products
                .stream()
                .filter(product -> product.getName().equals(productName))
                .findFirst();
    }

    int getQuantityOfAllProducts(){
        return products
                .stream()
                .reduce(0,(total,product)->total+product.getQuantity(),Integer::sum);
    }

    void increaseQuantityOfProduct(Product productToChange, int quantityToAdd){
        products.remove(productToChange);
        Product productToSave = new Product(productToChange.getName(), productToChange.getPrice(),productToChange.getQuantity()+quantityToAdd);
        products.add(productToSave);
    }

    void decreaseQuantityOfProduct(Product productToChange,int quantityToRemove){
        products.remove(productToChange);
        Product productToSave = new Product(productToChange.getName(), productToChange.getPrice(),productToChange.getQuantity()-quantityToRemove);
        products.add(productToSave);
    }

    int getTotalPrice(){
        return products
                .stream()
                .reduce(0,(total,product)->total+(product.getPrice()*product.getQuantity()),Integer::sum);
    }

    List<Product> findAll(){
       return List.copyOf(products);
    }

}
